note
	description: "Summary description for {DRONE_SPECIFICATION}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class DRONE_SPECIFICATION
inherit
  DRONE_TRANSPORT_REQUIREMENTS
  	select avoid_obstacle end
  DRONE_DROP_REQUIREMENTS
feature

  main_loop
  note
  	doc: "{DOCUMENTATION}.main_loop"
  	see: "{DRONE_REQUIREMENTS}.take_a_package_in_charge"
  	see: "{DRONE_REQUIREMENTS}.transport_package"
  	see: "{DRONE_DROP_REQUIREMENTS}.drop_package"
  	see: "{DRONE_REQUIREMENTS}.return_to_warehouse"
  do
    if assigned_package /= -1 then
      if attached_package /= assigned_package then
        take_a_package_in_charge
      else
        if location /= destination_location then
          transport_package
        else
          drop_package
        end
      end
    else
      if location /= warehouse_location then
        return_to_warehouse
      end
    end
  end

end
