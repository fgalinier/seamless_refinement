note
	description: "Summary description for {DRONE_TRANSPORT_REQUIREMENTS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class DRONE_TRANSPORT_REQUIREMENTS
inherit
  DRONE_REQUIREMENTS
  rename
    transport_safely_the_package as avoid_obstacle
  redefine
  	avoid_obstacle
  end
feature

  -- Require the drone to
  avoid_obstacle
  -- when an obstacle is detected
  note
  	see: "{DRONE_REQUIREMENTS}.transport_safely_the_package"
  require else
    obstacle_is_detected: obstacle_detected
  deferred
  ensure then
    no_obstacle: not obstacle_detected
  end
end
