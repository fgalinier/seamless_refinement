note
	description: "Summary description for {DOCUMENTATION}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	DOCUMENTATION
feature
	main_loop
	note
	doc: "[
		The DRONE is a reactive system. If a package is assigned to the drone, it should take the package in charge, transport it to the destination and return back to the warehouse.
		If no package is assigned, the DRONE shall go to its charge station.
	]"
	do end
end
