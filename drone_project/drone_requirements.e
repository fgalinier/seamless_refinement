note
	description: "Summary description for {DRONE_REQUIREMENTS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"
	EIS: "name=URD", "protocol=URI", "src=/home/ynigvi/drone_project/URD.docx"

deferred class DRONE_REQUIREMENTS
inherit
  DRONE_DOMAIN_KNOWLEDGE
feature
  -- Require the drone to
  take_a_package_in_charge
  -- when a package is assigned.
  note
	EIS: "name=URD", "protocol=URI", "src=/home/ynigvi/drone_project/URD.docx", "dest=1.1.1"
    doc: "[ When a package is assigned, the drone shall pick the right package. ]"
  	see: "{DRONE_SPECIFICATION}.main_loop"
  require
    a_package_is_assigned: assigned_package /= -1
  deferred
  ensure
    package_is_attached: attached_package = assigned_package
  end

  -- Require the drone to
  transport_package
  -- when a package is attached.
  note
	EIS: "name=URD", "protocol=URI", "src=/home/ynigvi/drone_project/URD.docx", "dest=1.1.2"
    doc: "[ When a package is attached, the drone shall go to the destination location. ]"
  	see: "{DRONE_SPECIFICATION}.main_loop"
  require
    package_is_attached: attached_package /= -1
  deferred
  ensure
    at_destination: location = destination_location
  end

  -- Require the drone to
  drop_package
  -- when at destination.
  note
	EIS: "name=URD", "protocol=URI", "src=/home/ynigvi/drone_project/URD.docx", "dest=1.1.3"
    doc: "[ When at destination, the drone shall drop carefully the package. ]"
  	see: "{DRONE_DRONE_REQUIREMENTS}.drop_package"
  require
    at_destination: location = destination_location
  deferred
  ensure
    no_package_attached: attached_package = -1
  end

  -- Require the drone to
  return_to_warehouse
  -- when no package is assigned.
  note
	EIS: "name=URD", "protocol=URI", "src=/home/ynigvi/drone_project/URD.docx", "dest=1.1.4"
    doc: "[ When no package is assigned, the drone shall return to the warehouse. ]"
  	see: "{DRONE_SPECIFICATION}.main_loop"
  require
    no_package_assigned: assigned_package = -1
  deferred
  ensure
    is_in_warehouse: location = warehouse_location
  end

  -- Require the drone to
  transport_safely_the_package
  note
	EIS: "name=URD", "protocol=URI", "src=/home/ynigvi/drone_project/URD.docx", "dest=1.2"
    doc: "[ The drone shall transport safely the package (i.e., avoid obstacles and drop the package carefully. ]"
    deferred end
end
