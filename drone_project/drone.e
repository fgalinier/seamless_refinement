note
	description: "Summary description for {DRONE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	DRONE

inherit
	DRONE_SPECIFICATION
	redefine
		avoid_obstacle,
		take_a_package_in_charge,
		transport_package,
		return_to_warehouse,
		assigned_package,
		front_sensors,
		bottom_sensors,
		engine_fl,
		engine_fr,
		engine_bl,
		engine_br,
		speed,
		attached_package,
		location,
		destination_location,
		go_down,
		drop
	end

feature -- Domain knowledge
	avoid_obstacle do end
	take_a_package_in_charge do end
	transport_package do  end
	return_to_warehouse do  end
	assigned_package: INTEGER do Result := -1 end
	front_sensors: DOUBLE do Result := -1 end
	bottom_sensors: DOUBLE do Result := -1  end
	engine_fl: DOUBLE do Result := -1  end
	engine_fr: DOUBLE do Result := -1  end
	engine_bl: DOUBLE do Result := -1  end
	engine_br: DOUBLE do Result := -1 end
	speed: DOUBLE do Result := -1 end
	attached_package: INTEGER do Result := -1  end
	location: TUPLE[DOUBLE,DOUBLE] do Result := [0.0,0.0]  end
	destination_location: TUPLE[DOUBLE,DOUBLE] do Result := [0.0,0.0] end
	go_down do  end
	drop do  end
end
