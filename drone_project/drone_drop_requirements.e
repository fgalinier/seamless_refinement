note
	description: "Summary description for {DRONE_DROP_REQUIREMENTS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"


deferred class DRONE_DROP_REQUIREMENTS
inherit
  DRONE_REQUIREMENTS
  rename
    transport_safely_the_package as drop_package_carrefuly
  redefine
  	drop_package_carrefuly,
    drop_package
  end
feature

  -- Require the drone to
  drop_package_carrefuly
  -- when at destination
  note
  	see: "{DRONE_REQUIREMENTS}.transport_safely_the_package"
  	see: "{DRONE_DROP_REQUIREMENTS}.drop"
  require else
    correct_height: height < 0.1 -- less than 10 centimeters height
  do
  	drop
  end

  drop_package
  note
  	see: "{DRONE_SPECIFICATION}.main_loop"
  	see: "{DRONE_REQUIREMENTS}.drop_package"
  	see: "{DRONE_DROP_REQUIREMENTS}.drop_package_carrefully"
  	see: "{DRONE_DROP_REQUIREMENTS}.go_down"
  do
    from
    until height < 0.1
    loop
      go_down
    end
    drop_package_carrefuly
  end

  go_down
  require
    not_on_the_ground: height > 0
  deferred end

  drop
  deferred end
end
