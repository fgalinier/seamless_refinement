note
	description: "Summary description for {DRONE_DOMAIN_KNOWLEDGE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class DRONE_DOMAIN_KNOWLEDGE
feature
  assigned_package: INTEGER
  note
  doc: "[ Each package is identified by an id (an INTEGER strictly positive). For each drone, a unique package is assigned by its id. If no package is assigned, this value is equal to -1. ]"
  deferred end

  front_sensors: DOUBLE
  note
  doc: "[ There are two sensors in the front of the drone, that return a distance in meters. ]"
  deferred end

  bottom_sensors: DOUBLE
  note
  doc: "[ There are two sensors under the drone, that return a distance in meters. ]"
  deferred end

  engine_fl: DOUBLE
  note
  doc: "[ The power of the front left engine (in Watt). ]"
  deferred end

  engine_fr: DOUBLE
  note
  doc: "[ The power of the front right engine (in Watt). ]"
  deferred end

  engine_bl: DOUBLE
  note
  doc: "[ The power of the back left engine (in Watt). ]"
  deferred end

  engine_br: DOUBLE
  note
  doc: "[ The power of the back right engine (in Watt). ]"
  deferred end

  speed: DOUBLE
  note
  doc: "[ The speed can be compute based in the power of the engines ]"
  deferred end

  height: DOUBLE
  note
  doc: "[ The height of the drone is given by the bottom sensors. ]"
  do Result := bottom_sensors end

  obstacle_detected: BOOLEAN
  note
  doc: "[ An obstacle is detected if the drone detect something in less than two meters in front of it. ]"
  do Result := front_sensors < 2.0 end

  attached_package: INTEGER
  note
  doc: "[ A scanner shall read the QR code of the package that is attached to the drone and return the ID of the package to the drone. If no package is attached, it returns -1. ]"
  deferred end

  location: TUPLE[DOUBLE, DOUBLE]
  note
  doc: "[ A location returned by the GPS system. ]"
  deferred end

  warehouse_location: TUPLE[DOUBLE, DOUBLE]
  note
  doc: "[ The warehouse location. ]"
  do Result := [43.5617391, 1.468017199999963] end

  destination_location: TUPLE[DOUBLE, DOUBLE]
  note
  doc: "[ The destination location given by the central system based on the assigned package. ]"
  deferred end

end
